######################################################
#              Tcl/Tk Project Manager                #
#              Distributed under GPL                 #
# Author: Sergey Kalinin, http://nuk-svk.ru          #
######################################################

ABOUT

Tcl/Tk Project Manager is a full IDE for programming in TCL/Tk.
It includes a project and file manager, a source editor with
syntax highlighting and procedure navigation, a context-sensitive
help system, and much more.
Working an Unix (Linux tested) and Windows.

![ProjMman 0.4.5](https://nuk-svk.ru/wp-content/uploads/2018/02/projman_0.4.5_small.png "ProjMan 0.4.5")

INSTALLATION

See INSTALL

REQUIREMENTS

For UNIX-like OS
Tcl/Tk >= 8.6 http://tcl.tk
BWidget http://sourceforge.net/projects/tcllib
tcl-img - for image viewer

For WINDOWS
ActiveTcl http://tcl.activestate.com

CREDITS

Sergey Kalinin - main project programmer and coordinator
banzaj28@gmail.com
http://nuk-svk.ru

Laurent Riesterer - VisualREGEXP and TkDIFF+ parts
laurent.riesterer@free.fr
http://laurent.riesterer.free.fr

Alexander Danilov - rewrite all documentation
daapp@chat.ru

Alexandr Dederer (aka Korwin) - many features and bugs fixed
dederer-a@mail.ru







